package agh.ddd.enrollments.domain.events;

import agh.ddd.enrollments.domain.valueobject.EnrollmentId;

/**
 * @author mciolek
 */
public class MaxParticipantCountExceededEvent {
    private final EnrollmentId enrollmentId;


    public MaxParticipantCountExceededEvent(EnrollmentId enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public EnrollmentId getEnrollmentId() {
        return enrollmentId;
    }
}
