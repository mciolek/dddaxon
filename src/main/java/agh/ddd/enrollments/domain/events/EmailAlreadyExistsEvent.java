package agh.ddd.enrollments.domain.events;

import agh.ddd.enrollments.domain.valueobject.Email;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;

/**
 * @author mciolek
 */
public class EmailAlreadyExistsEvent {
    private EnrollmentId enrollmentId;
    private Email email;

    public EmailAlreadyExistsEvent(EnrollmentId enrollmentId, Email email) {
        this.enrollmentId = enrollmentId;
        this.email = email;
    }

    public EnrollmentId getEnrollmentId() {
        return enrollmentId;
    }

    public Email getEmail() {
        return email;
    }
}
