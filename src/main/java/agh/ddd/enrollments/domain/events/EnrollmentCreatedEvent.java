package agh.ddd.enrollments.domain.events;

import agh.ddd.enrollments.domain.valueobject.EnrollmentConfiguration;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;

/**
 * @author mciolek
 */
public class EnrollmentCreatedEvent {
    private final EnrollmentId enrollmentId;
    private final String title;
    private final EnrollmentConfiguration configuration;

    public EnrollmentCreatedEvent(EnrollmentId enrollmentId, String title, EnrollmentConfiguration configuration) {
        this.enrollmentId = enrollmentId;
        this.title = title;
        this.configuration = configuration;
    }

    public EnrollmentId getId() {
        return enrollmentId;
    }

    public String getTitle() {
        return title;
    }

    public EnrollmentConfiguration getConfiguration() {
        return configuration;
    }
}
