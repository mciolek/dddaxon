package agh.ddd.enrollments.domain.events;

import agh.ddd.enrollments.domain.valueobject.Email;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;

/**
 * @author mciolek
 */
public class ParticipantNameChangedEvent {
    private final EnrollmentId enrollmentId;
    private final Email email;
    private final String firstName;
    private final String lastName;

    public ParticipantNameChangedEvent(EnrollmentId enrollmentId, Email email, String firstName, String lastName) {
        this.enrollmentId = enrollmentId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Email getEmail() {
        return email;
    }
}
