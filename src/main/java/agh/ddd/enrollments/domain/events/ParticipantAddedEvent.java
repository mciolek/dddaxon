package agh.ddd.enrollments.domain.events;

import agh.ddd.enrollments.domain.Participant;
import agh.ddd.enrollments.domain.valueobject.Email;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;

/**
 * @author mciolek
 */
public class ParticipantAddedEvent {
    private final Email email;
    private final String firstName;
    private final String lastName;
    private final EnrollmentId enrollmentId;

    public ParticipantAddedEvent(EnrollmentId id, Email email, String firstName, String lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.enrollmentId = id;
    }

    public Email getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public EnrollmentId getEnrollmentId() {
        return enrollmentId;
    }
}
