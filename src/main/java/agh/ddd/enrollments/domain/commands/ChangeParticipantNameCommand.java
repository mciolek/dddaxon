package agh.ddd.enrollments.domain.commands;

import agh.ddd.enrollments.domain.valueobject.Email;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;
import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

/**
 * @author mciolek
 */
public class ChangeParticipantNameCommand {
    @TargetAggregateIdentifier
    private final EnrollmentId enrollmentId;
    private final Email email;
    private final String firstName;
    private final String lastName;

    public ChangeParticipantNameCommand(EnrollmentId enrollmentId, Email email, String firstName, String lastName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.enrollmentId = enrollmentId;
    }

    public EnrollmentId getEnrollmentId() {
        return enrollmentId;
    }

    public Email getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
