package agh.ddd.enrollments.domain.commands;

import agh.ddd.enrollments.domain.valueobject.Email;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;
import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

/**
 * @author mciolek
 */
public class ParticipateInEnrollmentCommand {
    @TargetAggregateIdentifier
    private EnrollmentId enrollmentId;
    private Email email;
    private String firstName;
    private String lastName;

    public ParticipateInEnrollmentCommand(EnrollmentId enrollmentId, Email email, String firstName, String lastName) {
        this.enrollmentId = enrollmentId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public EnrollmentId getEnrollmentId() {
        return enrollmentId;
    }

    public Email getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
