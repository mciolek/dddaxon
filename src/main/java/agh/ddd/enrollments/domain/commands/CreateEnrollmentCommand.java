package agh.ddd.enrollments.domain.commands;

import agh.ddd.enrollments.domain.valueobject.EnrollmentConfiguration;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;
import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

/**
 * @author mciolek
 */
public class CreateEnrollmentCommand {
    @TargetAggregateIdentifier
    private final EnrollmentId enrollmentId;

    private final String title;

    private final EnrollmentConfiguration configuration;

    public CreateEnrollmentCommand(EnrollmentId enrollmentId, String title, EnrollmentConfiguration configuration) {
        this.enrollmentId = enrollmentId;
        this.title = title;
        this.configuration = configuration;
    }

    public EnrollmentId getId() {
        return enrollmentId;
    }

    public String getTitle() {
        return title;
    }

    public EnrollmentConfiguration getConfiguration() {
        return configuration;
    }
}
