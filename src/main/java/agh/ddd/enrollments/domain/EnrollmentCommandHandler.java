package agh.ddd.enrollments.domain;

import agh.ddd.enrollments.domain.commands.ChangeParticipantNameCommand;
import agh.ddd.enrollments.domain.commands.CreateEnrollmentCommand;
import agh.ddd.enrollments.domain.commands.ParticipateInEnrollmentCommand;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author mciolek
 */
@Component
public class EnrollmentCommandHandler {
    private Repository<Enrollment> enrollmentRepository;


    @CommandHandler
    public void handleCreateEnrollmentCommand(CreateEnrollmentCommand command){
        Enrollment enrollment = EnrollmentFactory.create(command.getId(), command.getTitle(), command.getConfiguration());
        enrollmentRepository.add(enrollment);
    }

    @CommandHandler
    public void handleParticipateInEnrollmentCommand(ParticipateInEnrollmentCommand command){
        Enrollment enrollment = enrollmentRepository.load(command.getEnrollmentId());
        enrollment.participantInEnrollment(command.getEmail(), command.getFirstName(), command.getLastName());
    }

    @CommandHandler
    public void handleChangeParticipantNameCommand(ChangeParticipantNameCommand command){
        Enrollment enrollment = enrollmentRepository.load(command.getEnrollmentId());
        enrollment.changeParticipantName(command.getEmail(), command.getFirstName(), command.getLastName());
    }


    @Autowired
    @Qualifier("enrollmentRepository")
    public void setEnrollmentRepository(Repository<Enrollment> enrollmentRepository) {
        this.enrollmentRepository = enrollmentRepository;
    }
}
