package agh.ddd.enrollments.domain;

import agh.ddd.enrollments.domain.valueobject.EnrollmentConfiguration;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;

/**
 * @author mciolek
 */
public class EnrollmentFactory {

    public static Enrollment create(EnrollmentId enrollmentId, String title, EnrollmentConfiguration enrollmentConfiguration){
        return new Enrollment(enrollmentId, title, enrollmentConfiguration);
    }
}
