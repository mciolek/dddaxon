package agh.ddd.enrollments.domain;

import agh.ddd.enrollments.domain.events.ParticipantNameChangedEvent;
import agh.ddd.enrollments.domain.valueobject.Email;
import org.axonframework.eventsourcing.EventSourcedEntity;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedEntity;
import org.axonframework.eventsourcing.annotation.EventSourcingHandler;

/**
 * @author mciolek
 */
public class Participant extends AbstractAnnotatedEntity {
    private final Email email;
    private String firstName;
    private String lastName;

    public Participant(Email email, String firstName, String lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Email getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @EventSourcingHandler
    private void handleChangeNameEvent(ParticipantNameChangedEvent event) {
        if (!event.getEmail().equals(email)) {
            return;
        }
        this.firstName = event.getFirstName();
        this.lastName = event.getLastName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Participant that = (Participant) o;

        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return email != null ? email.hashCode() : 0;
    }
}
