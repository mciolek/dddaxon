package agh.ddd.enrollments.domain.valueobject;

/**
 * @author mciolek
 */
public class EnrollmentId {
    private final String id;

    private EnrollmentId(String id) {
        this.id = id;
    }

    public static EnrollmentId of(String id) {
        return new EnrollmentId(id);
    }

    //Equals should be implemented properly
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EnrollmentId that = (EnrollmentId) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    //HashCode should be implemented properly
    @Override
    public int hashCode() {
        return id.hashCode();
    }

    //toString should be implemented properly
    @Override
    public String toString() {
        return id;
    }
}
