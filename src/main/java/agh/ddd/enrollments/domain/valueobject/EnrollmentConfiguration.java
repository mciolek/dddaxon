package agh.ddd.enrollments.domain.valueobject;

/**
 * @author mciolek
 */
public class EnrollmentConfiguration {
    private final int maxParticipantsCount;

    private EnrollmentConfiguration(int maxParticipantsCount) {
        this.maxParticipantsCount = maxParticipantsCount;
    }

    public int getMaxParticipantsCount() {
        return maxParticipantsCount;
    }

    public static EnrollmentConfigurationBuilder build(){
        return new EnrollmentConfigurationBuilder();
    }

    public static class EnrollmentConfigurationBuilder {
        private int maxParticipantsCount;

        public EnrollmentConfigurationBuilder participantsCount(int maxParticipantsCount) {
            this.maxParticipantsCount = maxParticipantsCount;
            return this;
        }

        public EnrollmentConfiguration get() {
            return new EnrollmentConfiguration(maxParticipantsCount);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EnrollmentConfiguration that = (EnrollmentConfiguration) o;

        if (maxParticipantsCount != that.maxParticipantsCount) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return maxParticipantsCount;
    }
}
