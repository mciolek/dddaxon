package agh.ddd.enrollments.domain.valueobject;

import com.google.common.base.Preconditions;

/**
 * @author mciolek
 */
public class Email {
    private final String value;

    private Email(String value) {
        Preconditions.checkNotNull(value);
        this.value = value;
    }

    public static Email of(String value){
        return new Email(value);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Email email = (Email) o;

        if (!value.equals(email.value)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }
}
