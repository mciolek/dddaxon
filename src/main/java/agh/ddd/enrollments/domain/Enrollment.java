package agh.ddd.enrollments.domain;

import agh.ddd.enrollments.domain.events.*;
import agh.ddd.enrollments.domain.valueobject.Email;
import agh.ddd.enrollments.domain.valueobject.EnrollmentConfiguration;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;
import org.axonframework.eventsourcing.annotation.AggregateIdentifier;
import org.axonframework.eventsourcing.annotation.EventSourcedMember;
import org.axonframework.eventsourcing.annotation.EventSourcingHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mciolek
 */
public class Enrollment extends AbstractAnnotatedAggregateRoot {
    @AggregateIdentifier
    private EnrollmentId id;
    private String title;
    private EnrollmentConfiguration configuration;
    @EventSourcedMember
    private List<Participant> participants = new ArrayList<>();


    private Enrollment() {
    }


    public Enrollment(EnrollmentId enrollmentId, String title, EnrollmentConfiguration configuration) {
        apply(new EnrollmentCreatedEvent(enrollmentId, title, configuration));
    }


    public void participantInEnrollment(Email email, String firstName, String lastName) {
        boolean emailExists = participants.stream()
                .anyMatch(participant -> participant.getEmail().equals(email));

        if (emailExists) {
            apply(new EmailAlreadyExistsEvent(id, email));
            return;
        }

        if (participants.size() >= configuration.getMaxParticipantsCount()) {
            apply(new MaxParticipantCountExceededEvent(id));
            return;
        }


        apply(new ParticipantAddedEvent(id, email, firstName, lastName));
    }

    public void changeParticipantName(Email email, String firstName, String lastName) {
        apply(new ParticipantNameChangedEvent(id, email, firstName, lastName));
    }

    @EventSourcingHandler
    public void onEnrollmentCreated(EnrollmentCreatedEvent event) {
        this.id = event.getId();
        this.title = event.getTitle();
        this.configuration = event.getConfiguration();
    }

    @EventSourcingHandler
    public void onParticipantAddedToEnrollment(ParticipantAddedEvent event) {
        Participant participant = new Participant(event.getEmail(), event.getFirstName(), event.getLastName());
        participants.add(participant);
    }

}