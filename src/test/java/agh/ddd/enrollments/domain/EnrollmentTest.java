package agh.ddd.enrollments.domain;


import agh.ddd.enrollments.domain.commands.ChangeParticipantNameCommand;
import agh.ddd.enrollments.domain.commands.CreateEnrollmentCommand;
import agh.ddd.enrollments.domain.commands.ParticipateInEnrollmentCommand;
import agh.ddd.enrollments.domain.events.*;
import agh.ddd.enrollments.domain.valueobject.Email;
import agh.ddd.enrollments.domain.valueobject.EnrollmentConfiguration;
import agh.ddd.enrollments.domain.valueobject.EnrollmentId;
import org.axonframework.test.FixtureConfiguration;
import org.axonframework.test.Fixtures;
import org.junit.Before;
import org.junit.Test;

public class EnrollmentTest {

    public static final int MAX_PARTICIPANTS_COUNT = 2;
    private FixtureConfiguration fixture;
    private EnrollmentConfiguration enrollmentConfiguration;
    private EnrollmentId enrollmentId;
    private Email email;

    @Before
    public void setUp() throws Exception {
        fixture = Fixtures.newGivenWhenThenFixture(Enrollment.class);

        EnrollmentCommandHandler commandHandler = new EnrollmentCommandHandler();
        commandHandler.setEnrollmentRepository(fixture.getRepository());
        fixture.registerAnnotatedCommandHandler(commandHandler);

        enrollmentConfiguration = EnrollmentConfiguration.build().participantsCount(MAX_PARTICIPANTS_COUNT).get();
        enrollmentId = EnrollmentId.of("1234");
        email = Email.of("jan@kowalski.pl");
    }

    @Test
    public void createEnrollmentCommandEventShouldCreateNewEnrollment() throws Exception {
        fixture.given()
                .when(
                        new CreateEnrollmentCommand(EnrollmentId.of("1234"), "Sample enrollment", enrollmentConfiguration)
                )
                .expectEvents(
                        new EnrollmentCreatedEvent(EnrollmentId.of("1234"), "Sample enrollment", enrollmentConfiguration)
                );

    }

    @Test
    public void participateEnrollmentCommandShouldAddParticipantToEnrollment() {
        fixture
                .given(
                        new EnrollmentCreatedEvent(enrollmentId, "Sample enrollment", enrollmentConfiguration)
                )
                .when(
                        new ParticipateInEnrollmentCommand(enrollmentId, email, "Jan", "Kowalski")
                )
                .expectEvents(
                        new ParticipantAddedEvent(enrollmentId, email, "Jan", "Kowalski")
                );
    }

    @Test
    public void participateEnrollmentCommandShouldNotAllowToDuplicateEmails() {
        fixture
                .given(
                        new EnrollmentCreatedEvent(enrollmentId, "Sample enrollment", enrollmentConfiguration),
                        new ParticipantAddedEvent(enrollmentId, email, "Arek", "Czarny")
                )
                .when(
                        new ParticipateInEnrollmentCommand(enrollmentId, email, "Jan", "Kowalski")
                )
                .expectEvents(
                        new EmailAlreadyExistsEvent(enrollmentId, email)
                );
    }

    @Test
    public void participateEnrollmentCommandShouldNotAllowToAddTooManyParticipants() {
        fixture
                .given(
                        new EnrollmentCreatedEvent(enrollmentId, "Sample enrollment", enrollmentConfiguration),
                        new ParticipantAddedEvent(enrollmentId, Email.of("jan@kowaslki.pl"), "Jan", "Kowalski"),
                        new ParticipantAddedEvent(enrollmentId, Email.of("arek@czarny.pl"), "Arek", "Czarny")
                )
                .when(
                        new ParticipateInEnrollmentCommand(enrollmentId, Email.of("johny@bravo.com"), "Johny", "Bravo")
                )
                .expectEvents(
                        new MaxParticipantCountExceededEvent(enrollmentId)
                );
    }

    @Test
    public void changeParticipantNameCommandShouldChangeNameIfParticipantExists() {
        fixture
                .given(
                        new EnrollmentCreatedEvent(enrollmentId, "Sample enrollment", enrollmentConfiguration),
                        new ParticipantAddedEvent(enrollmentId, email, "Jan", "Kowalski")
                )
                .when(
                        new ChangeParticipantNameCommand(enrollmentId, email, "Johny", "Bravo")
                )
                .expectEvents(
                        new ParticipantNameChangedEvent(enrollmentId, email, "Johny", "Bravo")
                );
    }
}